#!/usr/bin/env python

import zmq

def main():
    context = zmq.Context()
    socket = context.socket(zmq.SUB)

    # this would probably be a connect in the future
    socket.bind("tcp://*:5556")
    socket.setsockopt(zmq.SUBSCRIBE, "PS")

    while True:
        string = socket.recv()
        print string
        if string is "PS-EXIT":
            break

if __name__ == "__main__":
    main()
