#include <stdio.h>
#include <unistd.h>

#include <zmq.hpp>

#include <gphoto2/gphoto2.h>

// TODO: Move this to a header file
class PortInfoList {
    public:
        PortInfoList();
        ~PortInfoList();

        int get_count();
        int get_info(int index, GPPortInfo *info);

    private:
        GPPortInfoList *m_list;
        bool have_list;
};

PortInfoList::PortInfoList() {
    int result = gp_port_info_list_new(&(this->m_list));
    if (result < GP_OK) {
        printf("Fuck\n");
        this->m_list = NULL;
    }
    result = gp_port_info_list_load (this->m_list);
    if (result < GP_OK) {
        this->have_list = false;
    } else {
        this->have_list = true;
    }
}

PortInfoList::~PortInfoList() {
    if (this->m_list != NULL) {
        gp_port_info_list_free(this->m_list);
        this->m_list = NULL;
    }
}

int PortInfoList::get_count() {
    if (this->m_list == NULL || !this->have_list) {
        return -1;
    }

    int retval = gp_port_info_list_count(this->m_list);

    return retval;
}

int PortInfoList::get_info(int index, GPPortInfo *info) {
    if (this->m_list == NULL || !this->have_list) {
        return -1;
    }

    int result = gp_port_info_list_get_info(this->m_list, index, info);
    return result;
}

#define PORT_AVAILABLE_COMMAND "PS-PORT-AVAILABLE"
#define PORT_AVAILABLE_COMMAND_LENGTH 17

int main()
{
    // Build the port info list
    PortInfoList list;
    GPPortInfo info;

    int count = list.get_count();

    if (count < 0) {
        printf("Unable to get list: Error!\n");
        return -1;
    }

    zmq::context_t context(1);
    zmq::socket_t socket(context, ZMQ_PUB);
    socket.connect("tcp://localhost:5556");
    sleep(1); // ZeroMQ has some timing issues

    for(int i=0;i<count;i++) {
        char *name, *path;
        int total_message_length = PORT_AVAILABLE_COMMAND_LENGTH+4; // Account for separators and NULL terminator

        // STUB: Hostname will equal '*' for now.
        ++total_message_length;

        int result = list.get_info(i, &info);
        if (result < 0) {
            break;
        }
        gp_port_info_get_name(info, &name);
        gp_port_info_get_path(info, &path);

        total_message_length += strlen(name) + strlen(path);
        zmq::message_t response(total_message_length);
        snprintf((char *)response.data(), total_message_length,
          "%s|*|%s|%s", PORT_AVAILABLE_COMMAND, path, name
        );
        printf("Sending: %s\n", (char *)response.data());
        socket.send(response);
    }

}
